using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Ignis.GitLab.Api.Client.Http.Mocks;
using PowerAssert;
using Xunit;
using Xunit.Abstractions;
using static Ignis.GitLab.Api.Client.Http.Mocks.Request.MockHttpRequest;
using static Ignis.GitLab.Api.Client.Http.Mocks.Response.MockHttpResponse;

namespace Ignis.GitLab.Api.Client;

public sealed class GitLabApiClientFailedTest : IDisposable
{
    private readonly GitLabApiClient _client;
    private readonly MockHttpClient _mocks;

    public GitLabApiClientFailedTest(ITestOutputHelper output)
    {
        _mocks = new MockHttpClient();
        _client = new GitLabApiClient(_mocks.CreateHttpClient(output));
    }

    public void Dispose()
    {
        _client.Dispose();
    }

    [Fact]
    public async Task TestFailed()
    {
        _mocks.Setup(Get("https://gitlab.com/api/v4/version"), Fail(HttpStatusCode.InternalServerError));

        await PAssert.Throws<HttpRequestException>(() => _client.Version.GetAsync());
    }
}
