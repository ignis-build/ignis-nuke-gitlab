using PowerAssert;
using Xunit;

namespace Ignis.GitLab.Api.Client.Releases.NuGet;

public sealed class NuGetAssetLinkBuilderTest
{
    [Fact]
    public void TestBuild()
    {
        var actual = new NuGetAssetLinkBuilder()
            .UseNuGetOrg()
            .Version("1.4.6")
            .AddPackages(new[] {"Ignis.Build", "Ignis.Sample"})
            .Build();

        var expected = new AssetsLinks(new[]
        {
            new AssetsLink("Ignis.Build", "https://www.nuget.org/packages/Ignis.Build/1.4.6", "/Ignis.Build", "link"),
            new AssetsLink("Ignis.Sample", "https://www.nuget.org/packages/Ignis.Sample/1.4.6", "/Ignis.Sample", "link")
        });

        PAssert.IsTrue(() => actual.Equals(expected));
    }
}
