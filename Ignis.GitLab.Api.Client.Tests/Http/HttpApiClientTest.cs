using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Ignis.GitLab.Api.Client.Http.Mocks;
using PowerAssert;
using Xunit;
using Xunit.Abstractions;
using static Ignis.GitLab.Api.Client.Http.Mocks.Request.MockHttpRequest;
using static Ignis.GitLab.Api.Client.Http.Mocks.Response.MockHttpResponse;

namespace Ignis.GitLab.Api.Client.Http;

public sealed class HttpApiClientTest : IDisposable
{
    private readonly HttpClient _client;
    private readonly MockHttpClient _mocks;
    private readonly HttpApiClient _target;

    public HttpApiClientTest(ITestOutputHelper output)
    {
        _mocks = new MockHttpClient();
        _client = _mocks.CreateHttpClient(output);
        _client.BaseAddress = new Uri("https://gitlab.com");
        _target = new HttpApiClient(_client);
    }

    public void Dispose()
    {
        _client.Dispose();
    }

    [Fact]
    public async Task TestFailedGetAsync()
    {
        _mocks.Setup(Get("https://gitlab.com/api/v4/version"), Fail(HttpStatusCode.InternalServerError));

        var target = _target.Path("version");

        await PAssert.Throws<HttpRequestException>(() => target.GetAsync());
    }

    [Fact]
    public async Task TestPostAsync()
    {
        _mocks.Setup(Post("https://gitlab.com/api/v4/projects/foo%2Fbar", new {Value = "abc"}));

        var target = _target.Path("projects").Path("foo/bar");

        await target.PostAsync(new {Value = "abc"});
    }

    [Fact]
    public async Task TestFailedPostAsync()
    {
        _mocks.Setup(Post("https://gitlab.com/api/v4/projects/foo%2Fbar", new {Value = "abc"}),
            Fail(HttpStatusCode.InternalServerError));

        var target = _target.Path("projects").Path("foo/bar");

        await PAssert.Throws<HttpRequestException>(() => target.PostAsync(new {Value = "abc"}));
    }

    [Fact]
    public async Task TestPutAsync()
    {
        _mocks.Setup(Put("https://gitlab.com/api/v4/projects/foo%2Fbar", "hello"));

        var target = _target.Path("projects").Path("foo/bar");

        await target.PutAsync("hello");
    }

    [Fact]
    public async Task TestFailedPutAsync()
    {
        _mocks.Setup(Put("https://gitlab.com/api/v4/projects/foo%2Fbar", "hello"),
            Fail(HttpStatusCode.InternalServerError));

        var target = _target.Path("projects").Path("foo/bar");

        await PAssert.Throws<HttpRequestException>(() => target.PutAsync("hello"));
    }

    [Fact]
    public async Task TestDeleteAsync()
    {
        _mocks.Setup(Delete("https://gitlab.com/api/v4/projects/foo%2Fbar"), Fail(HttpStatusCode.InternalServerError));

        var target = _target.Path("projects").Path("foo/bar");

        await PAssert.Throws<HttpRequestException>(() => target.DeleteAsync());
    }
}
