namespace Ignis.GitLab.Api.Client.Logging;

public interface ILogger
{
    void WriteLine(string text = "");
}
