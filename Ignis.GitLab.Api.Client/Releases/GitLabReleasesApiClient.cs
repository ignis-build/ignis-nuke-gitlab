using Ignis.GitLab.Api.Client.Http;

namespace Ignis.GitLab.Api.Client.Releases;

public sealed class GitLabReleasesApiClient
{
    private readonly HttpApiClient _client;

    internal GitLabReleasesApiClient(HttpApiClient client)
    {
        _client = client.Path("releases");
    }

    public GitLabReleaseApiClient this[string tagName] => new(_client, tagName);

    public async Task CreateAsync(CreateReleaseRequest request)
    {
        await _client.PostAsync(request);
    }
}
