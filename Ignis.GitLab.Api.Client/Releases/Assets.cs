// ReSharper disable UnusedMember.Global

namespace Ignis.GitLab.Api.Client.Releases;

public sealed record Assets(AssetsLinks Links)
{
    public Assets() : this(new AssetsLinks())
    {
    }

    public AssetsLinks Links { get; } = Links;
}
