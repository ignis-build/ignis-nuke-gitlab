namespace Ignis.GitLab.Api.Client.Releases.NuGet;

public sealed class NuGetAssetLinkBuilder
{
    private readonly IList<string> _packageNames = new List<string>();
    private Uri? _baseUrl;
    private string? _version;

    public NuGetAssetLinkBuilder UseNuGetOrg()
    {
        return BaseUri("https://www.nuget.org/packages/");
    }

    // ReSharper disable once MemberCanBePrivate.Global
    public NuGetAssetLinkBuilder BaseUri(string value)
    {
        return BaseUri(new Uri(value));
    }

    // ReSharper disable once MemberCanBePrivate.Global
    public NuGetAssetLinkBuilder BaseUri(Uri value)
    {
        _baseUrl = value;
        return this;
    }

    public NuGetAssetLinkBuilder Version(string value)
    {
        _version = value;
        return this;
    }

    // ReSharper disable once MemberCanBePrivate.Global
    // ReSharper disable once UnusedMethodReturnValue.Global
    public NuGetAssetLinkBuilder AddPackage(string packageName)
    {
        _packageNames.Add(packageName);
        return this;
    }

    public NuGetAssetLinkBuilder AddPackages(IEnumerable<string> packageNames)
    {
        foreach (var packageName in packageNames)
        {
            AddPackage(packageName);
        }

        return this;
    }

    public AssetsLinks Build()
    {
        return new AssetsLinks(_packageNames.Select(AssetsLink));
    }

    private AssetsLink AssetsLink(string name)
    {
        return new AssetsLink(name, NuGetPackageUrl(name), $"/{name}", "link");
    }

    private string NuGetPackageUrl(string name)
    {
        return new Uri(VerifiedBaseUri(), $"{name}/{VerifiedVersion()}").ToString();
    }

    private Uri VerifiedBaseUri()
    {
        if (_baseUrl == null) throw new InvalidOperationException("Require base url.");
        return _baseUrl;
    }

    private string VerifiedVersion()
    {
        if (_version == null) throw new InvalidOperationException("Require version.");
        return _version;
    }
}
