using System.Collections;
using System.Collections.Immutable;

namespace Ignis.GitLab.Api.Client.Releases;

public sealed class Milestones : IEquatable<Milestones>, IEnumerable<string>
{
    private readonly ImmutableArray<string> _items;

    public Milestones(IEnumerable<string> items)
    {
        _items = items.ToImmutableArray();
    }

    public IEnumerator<string> GetEnumerator()
    {
        return ((IEnumerable<string>) _items).GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public bool Equals(Milestones? other)
    {
        if (ReferenceEquals(null, other)) return false;
        if (ReferenceEquals(this, other)) return true;
        return _items.SequenceEqual(other._items);
    }

    public override bool Equals(object? obj)
    {
        return ReferenceEquals(this, obj) || obj is Milestones other && Equals(other);
    }

    public override int GetHashCode()
    {
        return _items.GetHashCode();
    }
}
