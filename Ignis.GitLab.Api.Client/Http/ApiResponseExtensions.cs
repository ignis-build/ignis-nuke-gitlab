namespace Ignis.GitLab.Api.Client.Http;

internal static class ApiResponseExtensions
{
    public static async Task<T> GetObjectAsync<T>(this Task<ApiResponse> task)
    {
        var response = await task;
        return await response.GetObjectAsync<T>();
    }
}
