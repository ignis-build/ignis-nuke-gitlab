using System.Collections;

namespace Ignis.GitLab.Api.Client.Authentication;

/// <summary>
///     `JOB-TOKEN:` credential.
/// </summary>
internal sealed class JobTokenCredential : Credential
{
    private readonly string _token;

    private JobTokenCredential(string token)
    {
        _token = token;
    }

    internal static Credential? TryGet(IDictionary variables)
    {
        var token = TryGetCiJobToken(variables);
        if (token == null) return null;

        return new JobTokenCredential(token);
    }

    internal override void InjectTo(HttpClient client)
    {
        client.DefaultRequestHeaders.Add("JOB-TOKEN", _token);
    }

    private static string? TryGetCiJobToken(IDictionary variables)
    {
        if (!variables.Contains("CI_JOB_TOKEN")) return null;
        return $"{variables["CI_JOB_TOKEN"]}";
    }
}
