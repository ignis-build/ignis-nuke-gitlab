namespace Ignis.GitLab.Api.Client.Authentication;

/// <summary>
///     No credential. (anonymous access)
/// </summary>
internal sealed class NoCredential : Credential
{
    private NoCredential()
    {
    }

    public static Credential Instance { get; } = new NoCredential();

    internal override void InjectTo(HttpClient client)
    {
    }
}
