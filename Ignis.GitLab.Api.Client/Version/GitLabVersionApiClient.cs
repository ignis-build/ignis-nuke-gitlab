using Ignis.GitLab.Api.Client.Http;

namespace Ignis.GitLab.Api.Client.Version;

public sealed class GitLabVersionApiClient
{
    private readonly HttpApiClient _client;

    internal GitLabVersionApiClient(HttpApiClient client)
    {
        _client = client.Path("version");
    }

    public async Task<GitLabVersion> GetAsync()
    {
        return await _client.GetAsync().GetObjectAsync<GitLabVersion>();
    }
}
