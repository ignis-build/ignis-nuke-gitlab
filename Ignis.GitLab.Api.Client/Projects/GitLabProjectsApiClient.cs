using Ignis.GitLab.Api.Client.Http;

namespace Ignis.GitLab.Api.Client.Projects;

public sealed class GitLabProjectsApiClient
{
    private readonly HttpApiClient _client;

    internal GitLabProjectsApiClient(HttpApiClient client)
    {
        _client = client.Path("projects");
    }

    public GitLabProjectApiClient this[ProjectId index] => new(_client, index);
}
