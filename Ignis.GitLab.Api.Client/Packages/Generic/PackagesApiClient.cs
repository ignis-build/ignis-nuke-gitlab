using Ignis.GitLab.Api.Client.Http;

namespace Ignis.GitLab.Api.Client.Packages.Generic;

public sealed class PackagesApiClient
{
    private readonly HttpApiClient _client;

    internal PackagesApiClient(HttpApiClient client)
    {
        _client = client;
    }

    public PackageApiClient this[string packageName] => new(_client, packageName);
}
