using Ignis.GitLab.Api.Client.Authentication;
using JetBrains.Annotations;
using Nuke.Common;

namespace Builds;

public interface IGitLab : INukeBuild
{
    [Parameter("GitLab Project ID. Default is `ignis-build/ignis-nuke-gitlab`.")]
    string GitLabProject => TryGetValue(() => GitLabProject) ?? "ignis-build/ignis-nuke-gitlab";

    [Parameter] [CanBeNull] protected PrivateTokenCredential GitLabToken => TryGetValue(() => GitLabToken);

    protected Credential GitLabCredential() => GitLabToken ?? Credential.Default();
}
