using System.Collections.Immutable;
using System.Linq;
using Nuke.Common;
using Nuke.Common.IO;
using Nuke.Common.Tooling;
using Nuke.Common.Tools.DotNet;
using static Nuke.Common.Tools.DotNet.DotNetTasks;
using Project = Nuke.Common.ProjectModel.Project;

namespace Builds;

interface IPackage : ICompile
{
    AbsolutePath NuPkgDirectory => OutputDirectory / "nupkg";

    ImmutableArray<Project> PackagedProjects => new[]
        {
            "Ignis.GitLab.Api.Client",
            "Ignis.Nuke.GitLab"
        }
        .Select(project => Solution.GetProject(project))
        .ToImmutableArray();

    Target Pack => _ => _
        .DependsOn(Compile)
        .Executes(() =>
        {
            DotNetPack(s => s
                .SetConfiguration(Configuration)
                .SetOutputDirectory(NuPkgDirectory)
                .SetVersion(GitVersion.NuGetVersionV2)
                .SetProperty("EmbedUntrackedSources", true)
                .SetProperty("IncludeSymbols", true)
                .SetProperty("SymbolPackageFormat", "snupkg")
                .SetProperty("PackageLicenseExpression", "MIT")
                .SetAuthors("Tomo Masakura")
                .SetPackageProjectUrl("https://gitlab.com/ignis-build/ignis-nuke-gitlab")
                .SetRepositoryUrl("https://gitlab.com/ignis-build/ignis-nuke-gitlab.git")
                .SetRepositoryType("git")
                .EnableNoBuild()
                .EnableNoRestore()
                .CombineWith(PackagedProjects, (settings, project) => settings.SetProject(project)));
        });
}
