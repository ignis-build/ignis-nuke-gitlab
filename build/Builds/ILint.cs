using Ignis.ReSharper.Reporter.InspectCode.Convert.CodeQuality;
using Ignis.ReSharper.Reporter.InspectCode.Convert.Summary;
using Ignis.ReSharper.Reporter.InspectCode.Validations.SeverityLevel;
using Ignis.ReSharper.Reporter.Nuke;
using Nuke.Common;
using Nuke.Common.IO;
using Nuke.Common.Tooling;
using Nuke.Common.Tools.ReSharper;
using static Ignis.ReSharper.Reporter.Nuke.ReSharperReporterTasks;
using static Nuke.Common.Tools.ReSharper.ReSharperTasks;

namespace Builds;

public interface ILint : ICompile
{
    private AbsolutePath InspectCodeFile => ReportDirectory / "resharper" / "inspect-code.xml";
    private AbsolutePath CodeQualityFile => ReportDirectory / "gitlab" / "code-quality.json";
    private AbsolutePath InspectCodeCacheDirectory => CacheDirectory / "inspect-code";

    Target Lint => _ => _
        .DependsOn(Compile)
        .Executes(() =>
        {
            ReSharperInspectCode(s => s
                .SetTargetPath(Solution)
                .SetOutput(InspectCodeFile)
                .SetProperty("Configuration", Configuration)
                .SetProcessArgumentConfigurator(args => args.Add("--no-build"))
                .SetCachesHome(InspectCodeCacheDirectory));
        });

    // ReSharper disable once UnusedMember.Global
    Target CodeQuality => _ => _
        .TriggeredBy(Lint)
        .Executes(() =>
        {
            ReSharperReport(s => s
                .SetInput(InspectCodeFile)
                .SetSeverity(EnsureSeverityLevel.All)
                .AddExport<CodeQualityConverter>(CodeQualityFile)
                .AddExport<SummaryConverter>());
        });
}
