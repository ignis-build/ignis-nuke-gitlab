using System;
using Nuke.Common;
using Nuke.Common.Tools.DotNet;
using static Nuke.Common.Tools.DotNet.DotNetTasks;

namespace Builds;

interface IPush : IPackage
{
    private const string NuGetApiKeyEnvironment = "NUGET_API_KEY";

    [Parameter($"NuGet API Key. Default, read from the NUGET_API_KEY environment variable.{NuGetApiKeyEnvironment}")]
    string NugetApiKey => TryGetValue(() => NugetApiKey) ??
                          Environment.GetEnvironmentVariable(NuGetApiKeyEnvironment);


    [Parameter("Push NuGet source. Default is `nuget.org`.")]
    string NugetSource => TryGetValue(() => NugetSource) ?? "nuget.org";

    // ReSharper disable once UnusedMember.Global
    Target Push => _ => _
        .DependsOn(Pack)
        .Executes(() =>
        {
            DotNetNuGetPush(s => s
                .SetTargetPath(NuPkgDirectory / "*.nupkg")
                .SetApiKey(NugetApiKey)
                .SetSource(NugetSource));
        });
}
