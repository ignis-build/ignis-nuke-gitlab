using Ignis.GitLab.Api.Client.Projects;

namespace Ignis.Nuke.GitLab.Projects;

public static class GitLabProjectSettingsExtensions
{
    public static T SetProjectId<T>(this T settings, ProjectId value)
        where T : GitLabProjectSettings
    {
        settings.ProjectId = value;
        return settings;
    }
}
