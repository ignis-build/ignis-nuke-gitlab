using System.Collections.Immutable;
using Ignis.GitLab.Api.Client;
using Ignis.GitLab.Api.Client.Releases;
using Ignis.Nuke.GitLab.Releases;
using Nuke.Common.Tooling;

// ReSharper disable once CheckNamespace
namespace Ignis.Nuke.GitLab;

// ReSharper disable once UnusedType.Global
public static partial class GitLabTasks
{
    public static Task<IReadOnlyCollection<Output>> GitLabCreateReleaseAsync(
        Configure<GitLabCreateReleaseSettings> configure)
    {
        return GitLabCreateReleaseAsync(configure(new GitLabCreateReleaseSettings()));
    }

    // ReSharper disable once MemberCanBePrivate.Global
    public static async Task<IReadOnlyCollection<Output>> GitLabCreateReleaseAsync(GitLabCreateReleaseSettings settings)
    {
        using var client = settings.GitLabClient();
        await ReleaseClient(client, settings).CreateAsync(settings.ToRequest());
        return ImmutableArray<Output>.Empty;
    }

    public static Task<IReadOnlyCollection<Output>> GitLabDeleteReleaseAsync(
        Configure<GitLabDeleteReleaseSettings> configure)
    {
        return GitLabDeleteReleaseAsync(configure(new GitLabDeleteReleaseSettings()));
    }

    // ReSharper disable once MemberCanBePrivate.Global
    public static async Task<IReadOnlyCollection<Output>> GitLabDeleteReleaseAsync(GitLabDeleteReleaseSettings settings)
    {
        using var client = settings.GitLabClient();
        await ReleaseClient(client, settings)[settings.TagName].DeleteAsync();
        return ImmutableArray<Output>.Empty;
    }

    private static GitLabReleasesApiClient ReleaseClient(GitLabApiClient client, GitLabReleaseSettings settings)
    {
        return client.Projects[settings.ProjectId].Releases;
    }
}
