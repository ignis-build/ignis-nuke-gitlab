using Ignis.GitLab.Api.Client.Releases;

namespace Ignis.Nuke.GitLab.Releases;

#nullable enable

public sealed class GitLabCreateReleaseSettings : GitLabReleaseSettings
{
    public string? ReleaseName { get; set; }
    public string? Description { get; set; }
    public DateTime? ReleasedAt { get; set; }
    public string? Ref { get; set; }
    public IList<string> Milestones { get; } = new List<string>();
    public AssetsSettings Assets { get; } = new();

    public CreateReleaseRequest ToRequest()
    {
        return new CreateReleaseRequest(TagName, ReleaseName, Description, Ref, ReleasedAt, GetMilestones(), Assets);
    }

    private Milestones? GetMilestones()
    {
        if (Milestones.Any()) return new Milestones(Milestones);
        return null;
    }
}
