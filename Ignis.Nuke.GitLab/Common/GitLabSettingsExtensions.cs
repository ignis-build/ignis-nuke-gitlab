using Ignis.GitLab.Api.Client.Authentication;

namespace Ignis.Nuke.GitLab.Common;

public static class GitLabSettingsExtensions
{
    // ReSharper disable once UnusedMember.Global
    public static T SetGitLabUri<T>(this T settings, string value)
        where T : GitLabSettings
    {
        return settings.SetGitLabUri(new Uri(value));
    }

    // ReSharper disable once MemberCanBePrivate.Global
    public static T SetGitLabUri<T>(this T settings, Uri value)
        where T : GitLabSettings
    {
        settings.GitLabUri = value;
        return settings;
    }

    public static T SetHttpClient<T>(this T settings, Func<HttpClient> value)
        where T : GitLabSettings
    {
        settings.HttpClient = value;
        return settings;
    }

    public static T SetCredential<T>(this T settings, Credential value)
        where T : GitLabSettings
    {
        settings.Credential = value;
        return settings;
    }
}
